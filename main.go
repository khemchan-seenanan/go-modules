package main

import (
	"fmt"

	"khemchan-seenanan@bitbucket.org/khemchan-seenanan/go-modules/DataStructures/LinkedList"
)

func main() {
	var name string = "Khemchan Seenanan"
	fmt.Print(LinkedList.Greet(name))
}
